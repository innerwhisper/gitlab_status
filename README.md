# GitlabStatus

[![pipeline status](https://gitlab.com/innerwhisper/gitlab_status/badges/master/pipeline.svg)](https://gitlab.com/innerwhisper/gitlab_status/commits/master)

This gem provides CLI for check https://gitlab.com availability and measure response time.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'gitlab_status', git: 'https://gitlab.com/innerwhisper/gitlab_status.git'
```

And then execute:

    $ bundle

Or install via gitlab repository:
```
git clone https://gitlab.com/innerwhisper/gitlab_status.git
cd gitlab_status
bundle install
```

<!-- Or install it yourself as:

    $ gem install gitlab_status
 -->
## Usage

To check https://gitlab.com availability and measure response time, after installation run:

```
bundle exec cli check
```

To check https://gitlab.com availability and measure response time during 1 minute, after installation run:

```
bundle exec cli ping
```


## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `bundle exec rspec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitLab at https://gitlab.com/innerwhisper/gitlab_status. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the GitlabStatus project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://gitlab.com/innerwhisper/gitlab_status/blob/master/CODE_OF_CONDUCT.md).

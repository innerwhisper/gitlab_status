# frozen_string_literal: true

require 'gitlab_status/version'
require 'gitlab_status/request'

module GitlabStatus
  class << self
    def check
      performed_request
    end

    def check_average_response_time
      response_times = []

      probing_timepoints.each do |timepoint|
        wait_for_timepoint(timepoint)

        request = performed_request
        response_times << request.response_time
      end

      puts 'Average response time during pinging is'\
      " #{average_response_time(response_times).round(4)} seconds."
    end

    private

    def performed_request
      request = GitlabStatus::Request.new
      request.perform
      puts request.message
      request
    end

    def probing_timepoints
      time = Time.now
      [*0..5].map { |el| time + el * 10 }
    end

    def wait_for_timepoint(time)
      sleep(time - Time.now) if Time.now < time
    end

    def average_response_time(response_times)
      response_times.inject { |sum, el| sum + el }.to_f / response_times.size
    end
  end
end

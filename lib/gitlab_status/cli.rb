# frozen_string_literal: true

require 'thor'
require 'gitlab_status'

module GitlabStatus
  class CLI < Thor
    desc 'check', 'Check status of https://gitlab.com and return response time'
    def check
      GitlabStatus.check
    end

    desc 'ping', 'Probing status of https://gitlab.com several times'\
    ' and return average response time'
    def ping
      GitlabStatus.check_average_response_time
    end
  end
end

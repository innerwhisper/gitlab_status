# frozen_string_literal: true

require 'net/http'

module GitlabStatus
  class Request
    attr_reader :response_time, :message, :response

    def perform
      send_request_and_measure

      @message = case response
                 when Net::HTTPInformation, Net::HTTPSuccess, Net::HTTPRedirection
                   "#{url} is OK."\
                   " Response time is equal to #{response_time} seconds."
                 else
                   "#{url} is not OK."\
                   " Server response code is #{response.code}."\
                   " Response time is equal to #{response_time} seconds."
                 end
    rescue StandardError => e
      @message = "#{url} is not OK. Problem is: #{e}."
    end

    private

    def url
      'https://gitlab.com'
    end

    def send_request_and_measure
      time1 = Time.now
      @response = Net::HTTP.get_response(URI(url))
      @response_time = (Time.now - time1).round(4)
    end
  end
end

# frozen_string_literal: true

require 'spec_helper'

RSpec.describe GitlabStatus::Request do
  describe '#perform' do
    subject { described_class.new.perform }

    context 'when request is OK' do
      it { is_expected.to match(%r{https:\/\/gitlab.com is OK. Response time is equal to (\d)+.(\d){0,4} seconds.}) }
    end

    context 'when http status is 500' do
      before do
        stub_request(:any, 'https://gitlab.com/')
          .to_return(status: [500, 'Internal Server Error'])
      end

      it { is_expected.to match(%r{https:\/\/gitlab.com is not OK. Server response code is 500. Response time is equal to (\d)+.(\d){0,4} seconds.}) }
    end

    context 'when there is no internet connection (or another exception)' do
      before do
        stub_request(:get, 'https://gitlab.com/').to_raise(StandardError)
      end

      it { is_expected.to match('https:\/\/gitlab.com is not OK. Problem is:') }
    end

    context 'when timeout error was raised' do
      before do
        stub_request(:get, 'https://gitlab.com/').to_timeout
      end

      it { is_expected.to match('https:\/\/gitlab.com is not OK. Problem is:') }
    end
  end
end

# frozen_string_literal: true

require 'spec_helper'

RSpec.describe GitlabStatus do
  it 'has a version number' do
    expect(GitlabStatus::VERSION).not_to be nil
  end

  describe '.check' do
    it 'returns message from GitlabStatus::Request' do
      request = double('request')

      expect(GitlabStatus::Request).to receive(:new).and_return(request)
      expect(request).to receive(:perform)
      expect(request).to receive(:message)

      described_class.check
    end

    it { expect { described_class.check }.to output.to_stdout }
  end

  describe '.check_average_response_time' do
    before do
      Timecop.scale(3600)
    end

    it { expect { described_class.check_average_response_time }.to output(/Average response time during pinging is (\d)+.(\d){0,4} seconds./).to_stdout }
  end
end
